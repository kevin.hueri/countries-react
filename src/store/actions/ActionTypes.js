/*
 * Action types { ... }
 * ******************** */

// News
export const GET_NEWS_DATA = "GET_NEWS_DATA";
export const ADD_NEWS_DATA = "ADD_NEWS_DATA";

// Countries
export const GET_COUNTRIES_DATA = "GET_COUNTRIES_DATA";

// Weather
export const GET_WEATHER_DATA = "GET_WEATHER_DATA"