# WEBPACK

## install rapide REACT

### package.json

- Ajouter :

`"homepage": "./",`

- Ajouter dans "scripts":

`"w:build": "cross-env BUILD_PATH='./dist/webapp' react-scripts build",`

### Ligne de commande

`npm run w:build`

### Test

- Lancer le fichier `index.html` dans le dossier `dist/webapp`.
